# Introdução {#sec:introducao}

**_Share Your Media_** propõe uma solução para a partilha de ficheiros
multimédia num ambiente doméstico para que pessoas que moram na mesma casa
possam usufruir dos vídeos ou músicas que os seus colegas de casa decidiram
partilhar. O programa permite assim a existência de um pequeno servidor que
estará ligado e pronto a ser acedido pelos múltiplos dispositivos presentes na
mesma rede local.

A aplicação foi concebida considerando que um ou mais pessoas no domicílio
serão os administradores do sistema, sendo que estes possuem contas com
autorizações superiores que permitem registar novos utilizadores ou remover
utilizadores do sistema.

Os utilizadores de **_Share Your Media_** podem reproduzir _media_, podem dar
_upload_ de vídeos e músicas para que os seus colegas de casa possam também
consumir esse conteúdo, podem criar _playlists_ para agruparem as suas músicas
ou vídeos por _categoria_, _autor_ ou sem critério (referido na aplicação como
_random_). Os usuários podem ainda atribuir _categorias_ próprias a cada vídeo
ou música sendo que estas serão visíveis apenas ao próprio usuário.

Por último a aplicação contempla ainda a possibilidade de agrupar os conteúdos
por séries de televisão e álbuns de música.

A modelação do projeto foi construída com a UML, a Linguagem de Modelação
Unificada, para a elaboração de diagramas necessários à sua concepção.

A solução apresentada foi desenvolvida em Java 11 utilizando para a
interface gráfica a biblioteca _javafx_. A persistência dos dados foi
concretizada numa base de dados relacional. O motor da base de dados escolhido
foi o MySQL.

