## Protótipo Interface

![Painel de Boas-Vindas](figures/interfaces/welcome.png)

![Painel de Administrador](figures/interfaces/admin.png)

![Painel de LogIn](figures/interfaces/login.png)

![Painel Principal](figures/interfaces/main.png)

![Botão de Upload](figures/interfaces/upload.png)
