## Especificação de Use Cases {#sec:spec}


### Use Case 1: Adicionar utilizador

**Descrição:** Adicionar utilizador.

**Cenário:** Manuel chega a casa e quer participar no *Media Center*.

**Pré-condição:** O utilizador fornecer o nome, e-mail e password ao
administrador do sistema.

**Pós-condição:** O utilizador foi registado com sucesso e a sua _password_
definida.

**Fluxo Normal:**

  1. Um administrador fornece um nome, e-mail e _password_.
  2. O sistema verifica se os parâmetros são válidos.
  3. O sistema cria a conta com os parâmetros.

**Fluxo de Excepção [Email já se encontra registado] (Passo 2):**

  1. Sistema avisa que o email já se encontra registado.

**Fluxo de Excepção [_Password_ não é segura] (Passo 2):**

  1. Sistema avisa que a _password_ não é segura o suficiente.


### Use Case 2: Iniciar Sessão

**Descrição:** Iniciar sessão.

**Cenário:** Hugo inicia sessão no *Media Center.*

**Pré-condição:** Não há sessões iniciadas *Media Center*.

**Pós-condição:** O utilizador iniciou a sessão.

**Fluxo Normal:**

  1. O Utilizador fornece o e-mail e a password.
  2. O sistema verifica se o e-mail está registado.
  3. O sistema verifica se a _password_ fornecida é a correta.
  4. O sistema inicia a sessão.

**Fluxo de Excepção [E-mail não está registado] (Passo 2):**

  1. O sistema informa o utilizador que o e-mail não está registado.

**Fluxo de Excepção [Password não é a correta] (Passo 3)**:

  1. Sistema informa que a _password_ é incorreta.


### Use Case 3: Terminar Sessão

**Descrição:** Terminar sessão.

**Cenário:** Hugo termina sessão no *Media Center*.

**Pré-condição:** O utilizador tem a sessão iniciada.

**Pós-condição:** O utilizador não tem a sessão iniciada.

**Fluxo Normal:**

  1. Utilizador indica que quer encerrar a sessão.
  2. Sistema pede confirmação.
  3. Utilizador confirma.
  4. O sistema encerra a sessão.

**Fluxo de Excepção [Utilizador não confirma] (Passo 3):**

  1. Sistema indica que o término da sessão foi cancelado.


### Use Case 4: Editar Utilizador

**Descrição:** Editar utilizador.

**Cenário:** Ricardo edita a sua conta.

**Pré-condição:** O utilizador tem sessão iniciada.

**Pós-condição:** A informação da conta foi editada.

**Fluxo Normal:**

  1. O utilizador fornece a sua _password_.
  2. O utilizador indica ao sistema que pretende editar o seu e-mail.
  3. O utilizador fornece o novo e-mail.
  4. O sistema verifica se o novo e-mail se encontra disponível.
  5. O sistema edita a conta, com as alterações desejadas.

**Fluxo Alternativo [Utilizador pretende mudar a _password_] (Passo 2):**

  1. O utilizador fornece a nova _password_.
  2. O utilizador confirma a nova _password_.
  3. O sistema verifica se as passwords são iguais.
  4. Retoma o Fluxo Normal, no passo 8.

**Fluxo Alternativo [Utilizador pretende mudar o nome] (Passo 2):**

  1. O utilizador fornece o novo nome.
  2. Retoma o Fluxo Normal, no passo 8.

**Fluxo de Excepção [O e-mail não se encontra disponível] (Passo 4):**

  1. O sistema informa o utilizador que o novo e-mail não se encontra
     disponível.

**Fluxo de Excepção [_Password_ incorreta] (Passo 1):**

  1. O sistema informa o utilizador que a _password_ é incorreta.

**Fluxo de Excepção [_Passwords_ não são iguais] (Passo 3 do 1º Fluxo
Alternativo):**

  1. O sistema informa que as _password_ não são iguais.


### Use Case 5: Eliminar Utilizador

**Descrição:** Eliminar utilizador.

**Cenário:** Ricardo elimina uma conta.

**Pré-condição:** O utilizador tem sessão iniciada e é administrador.

**Pós-condição:** A conta foi eliminada.

**Fluxo Normal:**

  1. O utilizador fornece um e-mail.
  2. O utilizador fornece a sua _password_.
  3. O sistema verifica se o e-mail está registado.
  4. O sistema verifica se a _password_ fornecida é correta.
  5. O sistema elimina a conta.

**Fluxo de Excepção[A _password_ fornecida é incorreta] (Passo 4):**

  1. O sistema informa que a _password_ fornecida é incorreta.

**Fluxo de Excepção [E-mail não está registado] (Passo 3):**

  1. O sistema informa que a conta com aquele email não existe.


### Use Case 6: Upload de *Media Files*

**Descrição:** Upload de *Media Files*.

**Cenário:** Pedro faz upload da sua musica para o *Media Center*.

**Pré-condição:** O utilizador está registado e há espaço no *Media Center*
para os *Media Files.*

**Pós-condição:** Os *Media Files* estão no *Media Center* e todos os
*Media Files* carregados estão na coleção do utilizador.

**Fluxo Normal:**

  1. O utilizador fornece Media Files a adicionar.
  2. O sistema verifica que a extensão dos ficheiros é suportada.
  3. O sistema verifica se estes já se encontram no *Media Center*.
  4. Adiciona os *Media Files* à coleção do utilizador.

**Fluxo Alternativo [_Media Files_ não se encontram no _Media Center_] (Passo 3):**

  1. O sistema faz upload dos _Media Files_.
  2. Atribui-lhes categorias.
  3. Retoma o Fluxo Normal, no passo 4.

**Fluxo de Exceção [A extensão não é suportada] (Passo 2):**

  1. O sistema informa que o tipo de ficheiro que o utilizador está a tentar
     carregar não é suportado pelo *Media Center*.


### Use Case 7: Mudar a categoria de um *Media File*

**Descrição:** Mudar a categoria de *Media File*.

**Cenário:** O Nelson muda a categoria de uma música.

**Pré-condição:** O utilizador está registado e o *Media File* está no
*Media Center*.

**Pós-condição:** O *Media File* está categorizado como o utilizador
pretendia.

 **Fluxo Normal:**

  1. O utilizador fornece o nome do *Media File*.
  2. O utilizador escolhe as categorias às quais quer adicionar o ***Media
     File***.
  3. O sistema verifica se o _Media File_ está no _Media Center_.
  4. O sistema muda as categorias do _Media File_.

**Fluxo de Excepção [_Media File_ não está no _Media Center_] (Passo 3):**

  1. O sistema informa que o _Media File_ não consta do _Media Center_.


### Use Case 8: Remover *Media Files*

**Descrição:** Remover *Media Files*.

**Cenário:** Rui remove *Media Files* do *Media Center*.

**Pré-condição:** O utilizador tem sessão iniciada.

**Pós-condição:** Os Media Files foram eliminados do sistema.

**Fluxo Normal:**

  1. O utilizador fornece o nome do *Media File* a eliminar.
  2. O sistema verifica se o *Media File* está no Media Center.
  3. O sistema verifica se o *Media File* foi adicionado pelo utilizador.
  4. O sistema elimina o *Media File*.

**Fluxo de Exceção [O _Media File_ não foi adicionados pelo utilizador] (Passo
3):**

  1. O sistema informa o utilizador que o _media file_ que pretende eliminar não
     foi adicionado por ele.

**Fluxo de Excepção [O _Media File_ não está no _Media Center_] (Passo 2):**

  1. O sistema informa o utilizador que o *Media File* não consta do
     *Media Center*.


### Use Case 9: Reproduzir *Media Files*

**Descrição:** Reproduzir *Media Files*.

**Cenário:** O Rui reproduz a sua coleção de música.

**Pré-condição:** Existem *Media Files* no *Media Center*.

**Pós-condição:** Os *Media Files* foram reproduzidos.

**Fluxo Normal:**

  1. O utilizador escolhe os _Media Files_ a serem reproduzidos.
  2. O sistema verifica se o utilizador tem acesso aos *Media Files*
     escolhidos.
  3. O sistema reproduz estes *Media Files*.

**Fluxo de Exceção [O utilizador não tem acesso aos _Media Files_] (passo 2):**

  1. O sistema informa o utilizador que não tem acesso aos *Media Files*.


### Use Case 10: Criar *Playlist*

**Descrição:** Criar *Playlist*.

**Cenário:** O Nelson cria uma *playlist* com as músicas do seu artista favorito.

**Pré-condição:** O utilizador tem sessão iniciada.

**Pós-condição:** A *playlist* foi criada.

**Fluxo Normal:**

  1. O sistema pergunta qual o critério para a criação da *playlist*.
  2. O utilizador responde que quer uma *playlist* com o critério "aleatório".
  3. O sistema verifica se existem *Media Files* que satisfaçam o critério
     escolhido.
  4. O sistema cria a *playlist* com os *Media Files* que obedecem ao critério
     escolhido.

**Fluxo Alternativo [O utilizador responde que quer uma *playlist* com o critério
"por artista"] (Passo 2):**

  1. O utilizador fornece ao sistema o artista.
  2. Retoma o Fluxo Normal, no passo 3.

**Fluxo Alternativo [O utilizador responde que quer uma *playlist* com o critério
"por género"] (Passo 2):**

  1. O utilizador fornece ao sistema um género.
  2. Retoma ao Fluxo Normal, no passo 3.

**Fluxo de Exceção [Não existem *Media Files* que satisfaçam o critério escolhido]
(Passo 3):**

  1. O sistema informa o utilizador que não existem *Media Files* que satisfaçam o
     critério escolhido.

