## Diagramas de Sequência {#sec:sequence}

### Use Case 6 - Upload de _Media Files_

![Upload de _Media Files_](figures/diagrams/sequence/06.jpg)

### Use Case 7 - Mudar a categoria de um _Media File_

![Mudar a categoria de um _Media File_](figures/diagrams/sequence/07.jpg)
