## Diagrama de Classe {#sec:class}

![Diagrama de Classe](figures/diagrams/class_diagram.jpg){#fig:class}

No diagrama apresentado na @fig:class foram omitidos _getters_ e _setters_, uma
vez que, são necessários para a maioria das variáveis de instância e nenhuma
requer atenção especial.

