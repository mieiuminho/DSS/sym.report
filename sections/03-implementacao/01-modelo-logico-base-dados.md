# Implementação

## Modelo Lógico da Base de Dados {#sec:base_dados}

![Modelo lógico](figures/base_dados.png)

Conforme mencionado na Introdução a aplicação suporta Álbuns e Séries pelo que
necessitamos de uma tabela para cada um dos tipos de conteúdo (_ALBUM_ e
_SERIES_), sendo que cada tabela contém como chave primária: o nome do álbum ou
série, respetivamente (uma vez que um álbum ou uma série são constituídos por um
ou mais MediaFiles).

Existe uma tabela que guarda os MediaFiles (_MEDIAFILE_), isto é, guarda as
informações relativas aos MediaFiles (nome, artista, nome do álbum e nome da
série, sendo que se estivermos a falar de um álbum o nome da série é um _String_
vazia e o mesmo acontece para um episódio de uma série para a coluna do nome).

Temos também tabelas para os usuários normais e para os administradores
(_REGULAR_USER_ e _ADMIN_USER_) em que guardamos o seu email, nome, password
(uma versão da password depois de ter sido submetida a uma função de _hash_) e o
**salt** (_String_ que é adicionada à password antes do processo de _hash_ para
que passwords iguais gerem _hashs_ diferentes).

Os MediaFiles têm também categorias imutáveis que são atribuídas aquando do
_upload_ dos mesmos através da leitura dos seus _metadados_ e guardados na
tabela _DEFAULT_CATEGORIES_ que tem como _foreign keys_: nome do MediaFile e o
artista do MediaFile. Esta tabela armazena ainda as categorias estáticas do
MediaFile.

De acordo com as especificações da aplicação cada usuário pode atribuir
diferentes categorias a um MediaFile pelo que é necessária uma tabela para
guardar essa informação (_CUSTOM_CATEGORY_). Nesta tabela temos como _foreign
keys_: o nome do usuário, o nome do MediaFile e o artista do MediaFile (uma vez
que um usuário pode atribuir diferentes categorias a vários MediaFiles e cada
MediaFile pode ter diferentes categorias para diferentes usuários). Na presente
tabela armazenamos as categorias atribuídas pelo usuário (com um máximo de 3).

Cada usuário pode realizar diversos _uploads_ pelo que segue-se a mesma ideia da
tabela da tabela (_CUSTOM_CATEGORY_), por isso temos como _foreign keys_: o nome
do usuário que realizou o _upload_, o nome do MediaFile que foi carregado e o
artista do MediaFile carregado.

Um usuário pode ter diversas _playlists_ pelo que necessitamos de uma tabela
_PLAYLIST_ que tem como _primary key_ o nome da _playlist_ e como _foreign key_
o email do criador da mesma. É guardado ainda o critério através do qual é
gerada a _playlist_. Uma _playlist_ é constituída por vários MediaFiles e um
usuário pode ter diversas _playlists_ pelo que surge a necessidade de existir a
tabela _PLAYLIST_has_MEDIAFILE_ que tem como _foreign keys_: o nome da
_playlist_, o email do criador da mesma, o nome do MediaFile que integra a
_playlist_ e o artista do mesmo MediaFile.
