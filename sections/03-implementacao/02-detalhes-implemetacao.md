## Detalhes de Implementação {#sec:det_impl}

### Object Relational Mapping

Para a implementação dos _Data Acess Objects_ que tornam possível a interação com
as tabelas da base de dados optamos por desenvolver uma classe genérica chamada
`DataAccessObject` que fornece os métodos que necessitamos para a interação com
as tabelas. Esta classe genérica tem como variáveis de instância:

```java
private String table;
private List<String> columns;
private DataClass<K> token;
```

A primeira variável é o nome da tabela a que o _Data Access Object_ se refere, o
segundo é a lista dos nomes das colunas que essa tabela possui e a terceira é
uma classe na qual representaremos uma linha da tabela.

Para implementar as classes que permitem a persistência de dados criamos classes
que representam as linhas de tabelas, por exemplo: para a tabela das categorias
estáticas dos MediaFiles (_DEFAULT_CATEGORIES_) criamos a classe
`DefaultCategories` que representa uma linha da tabela mencionada e implementa
`DataClass<String>`. Para interagir com a tabela precisamos de criar, então, a
classe `CustomCategoriesDAO` que estende a classe `DataAcessObject<String,
CustomCategories>`. Para as restantes classes com funcionalidade semelhante a
implementação foi análoga.

### Arquitetura Cliente-Servidor

De forma a ser possível o acesso aos ficheiros pelos diferentes utilizadores é
necessário que quando estes façam _upload_ de uma nova música ou vídeo, a
mesma passe a existir num local acessível por todos.

A arquitetura implementada concebe a existência de uma máquina ligada na rede
doméstica com uma instância do servidor a correr. Nesta mesma máquina, é
necessário que esteja a correr o motor da base de dados.

Assim, cada utilizador passa a ligar-se através da aplicação desenvolvida a este
servidor que o capacita de partilhar ficheiros multimédia, assim como,
reproduzir qualquer uma das músicas ou vídeos adicionados por si ou pelos
restantes utilizadores.

Para que isso seja possível, o cliente cria uma conexão com o servidor através
de sockets TCP (Transmission Control Protocol) sempre que desejar reproduzir uma
música ou vídeo e quando se pretende fazer o _upload_ de novas _media_.

