## Diagrama de classe com DAOs {#sec:classeDAO}

![Diagrama de classe com DAO](figures/DiagramaDeClassesDAO.png)

É de notar que com a introdução dos _Data Access Objects_ acabamos por optar por
não usar a classe `UploadersInfo` que existia no diagrama de classes da segunda
fase do trabalho, uma vez que esta introduzia complexidade desnecessária e a
informação que esta classe guardaria encontra-se agora na tabela _UPLOAD_.
