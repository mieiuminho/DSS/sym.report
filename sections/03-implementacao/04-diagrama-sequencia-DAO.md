## Diagramas de sequência (considernado DAO's)

### Mudar as categorias de um MediaFile
![Mudar as categorias de um MediaFile](figures/MudarCategoriaMediaFileDAO.png)

### Upload de MediaFiles
![Upload de MediaFiles](figures/UploadMediaFilesDAO.png)
