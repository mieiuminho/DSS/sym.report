# Manual de utilização {#sec:man_util}

A utilização do programa **_Share Your Media_** começa no ecrã de login. O
utilizador deve autenticar-se com uma conta previamente criada ou utilizar o
programa como convidado.

Para carregar um _MediaFile_ o utilizador (devidamente autenticado) deve
selecionar a aba _Upload_, personalizar os campos de identificação do ficheiro
(_Name_, _Artist_, _Album_, _Series_) e de seguida pressionar o botão _Upload_,
selecionando do seu computador o ficheiro que pretende carregar para o sistema.

![Interface de Upload](figures/uploadScreen.png){#fig:upload}

Os utilizadores registados do programa podem selecionar a aba _Friends_ (na
@fig:instructions a verde) para consultar os seus amigos.

Os utilizadores registados do programa conseguirão, numa iteração futura do
programa, selecionar a aba _Playlists_ (na @fig:instructions a azul claro)
para consultarem, reproduzirem e partilharem as suas _Playlists_.

![Playlists e amigos](figures/instrucoes.png){#fig:instructions}

Existe ainda a funcionalidade de os usuários do programa selecionarem (através
de duplo clique) categorias de um _MediaFile_ e alterá-las ficando essas
associadas apenas ao usuário.

Por fim, os usuários da aplicação podem selecionar a aba _Account_ seguido de
_Logout_ para terminarem a sua sessão no programa.

