# Conclusão {#sec:conclusao}

Este projeto foi constituído por três partes. A primeira fase do projeto
envolveu a identificação de _use cases_ e especificação dos mesmos. Para a
segunda fase foram definidos, pelo corpo docente, 10 _use cases_, para os quais
teríamos de fazer as suas especificações e esboçar os diagramas de sequência.
Para a terceira fase do projeto começamos a implementação do sistema **_Share
Your Media_** e a implementação da persistência de dados, isto é, guardar os
dados do programa numa base de dados relacional.

O desenvolvimento dos diagramas de sequência ajudou a clarificar o processo de
desenvolvimento, na medida em que, à medida que vamos dissecando os _use cases_
e deduzimos a necessidade de existência de algumas classes derivam dos diagramas
alguns métodos que têm de existir para que a arquitetura que desenvolvemos seja
exequível para o projeto.

